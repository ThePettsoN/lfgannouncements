## Interface: 20502
## Title: LFG Announcements
## Notes: 
## Author: ThePettsoN
## Version: 1.1.0-ALPHA
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: LFGAnnouncementsDB

embeds.xml
src/gui/AceGUIContainer-CollapsableInlineGroup.lua
src/gui/AceGUIContainer-Toaster.lua
src/utils.lua
src/core.lua
src/db.lua
src/dungeons.lua
src/ui.lua
src/minimap_button.lua
src/notifications.lua

src/options/main.lua
src/options/templates/general.lua
src/options/templates/filter.lua